FROM centos:7 

RUN groupadd kingbase && useradd -g kingbase -m -d /home/kingbase -s /bin/bash kingbase

WORKDIR /home/kingbase

ARG installFilesDir=/home/kingbase

ARG installConfName=installer.properties

ARG version=V008R002C001B0464

ADD kingbase.tar.gz ./kdb/

ADD docker-entrypoint.sh ./

ADD installer.properties ./

RUN chmod +x docker-entrypoint.sh

RUN chown -R kingbase:kingbase /home/kingbase

ENV PATH /home/kingbase/kdb/Server/bin:$PATH

ENV DB_VERSION ${version}

USER kingbase

RUN sh ${installFilesDir}/kdb/KingbaseES_${version}_Lin64_install/setup.sh -i silent -f ${installFilesDir}/${installConfName}

EXPOSE 54321

ENTRYPOINT ["/home/kingbase/docker-entrypoint.sh"]
