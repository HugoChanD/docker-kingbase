# README #

人大金仓数据库容器

### 下载编译 ###

* git clone https://HugoChanD@bitbucket.org/HugoChanD/docker-kingbase.git
* cd docker-kingbase
* docker build -t hugochand/kingbase:latest .

### docker启动 ###

* docker run -d -p 54321:54321 -e SYSTEM_PWD=123456 -v /data/kingbase:/home/kingbase/data -v /data/kingbase/license.dat:/home/kingbase/kdb/Server/bin/license.dat hugochand/kingbase:latest
* license文件可以在人大金仓官网下载

### docker-compose 启动 ###

* docker-compose up -d

### 使用 ###

* 默认用户SYSTEM,通过环境变量SYSTEM_PWD指定初始化数据库时的默认用户密码(长度9-48)

